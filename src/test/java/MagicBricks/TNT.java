package MagicBricks;

import java.io.File;




import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TNT {

	@Test
	public void testBangladesh() throws Exception {
		System.setProperty("webdriver.chrome.driver",
				new File(System.getProperty("user.dir"))
						+ "/src/test/resources" + "/driver/"
						+ "chromedriver.exe");

		WebDriver driver = new ChromeDriver();
		driver.get("https://www.tnt.com/express/en_gc/site/home.html");
		driver.manage().window().maximize();
		Thread.sleep(3000);
		// driver.findElement(By.cssSelector(".js-r42-alerting-close")).click();
		Thread.sleep(3000);
		driver.findElement(By.className("c-modal__close")).click();
		
		driver.findElement(
				By.className("cms-c-countryselector__current__title")).click();

		Thread.sleep(3000);
		driver.findElement(By.linkText("Bangladesh")).click();
		Assert.assertTrue(driver.getCurrentUrl().contains("en_bd"));
		driver.quit();

	}

	@Test
	public void testUrl() throws Exception {
		System.setProperty("webdriver.chrome.driver",
				new File(System.getProperty("user.dir"))
						+ "/src/test/resources" + "/driver/"
						+ "chromedriver.exe");

		WebDriver driver = new ChromeDriver();
		driver.get("https://www.tnt.com/express/en_bd/site/home.html");
		driver.manage().window().maximize();
		Thread.sleep(3000);

		System.out.println(driver.findElement(
				By.className(" cms-c-countryselector__current__title"))
				.getText());
		Assert.assertEquals(
				"Bangladesh",
				driver.findElement(
						By.className(" cms-c-countryselector__current__title"))
						.getText());
		driver.quit();
	}

	@Test
	public void getTitle() {
		System.setProperty("webdriver.chrome.driver",
				new File(System.getProperty("user.dir"))
						+ "/src/test/resources" + "/driver/"
						+ "chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.tnt.com/express/en_bd/site/home.html");
		driver.manage().window().maximize();
		String actualvalue = driver.getTitle();
		System.out.println(actualvalue);    
		Assert.assertEquals("TNT Express Shipping | TNT Bangladesh",
				actualvalue);
		driver.quit();

	}

}
